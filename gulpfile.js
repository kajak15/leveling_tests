var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('sass', function () {
    return sass('./web/scss/main.scss', {sourcemap: true})
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: './web/scss'
        }))
        .pipe(gulp.dest('./web/css'));
});

gulp.task('watch', function() {
    gulp.watch('./web/scss/**/*', ['sass']);
});

gulp.task('default', ['sass', 'watch']);

