#!/bin/bash
# declare STRING variable




DIRECTORYNAME="web"
echo $DIRECTORYNAME
chmod 777 $DIRECTORYNAME
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME


DIRECTORYNAME="web/assets"
echo $DIRECTORYNAME
chmod 777 $DIRECTORYNAME
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME

DIRECTORYNAME="runtime"
echo $DIRECTORYNAME
chmod 777 $DIRECTORYNAME
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
chmod 777 $(whoami):$(whoami) $(pwd)$DIRECTORYNAME

DIRECTORYNAME="web/upload"
echo $DIRECTORYNAME
chmod 777 $DIRECTORYNAME
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" $DIRECTORYNAME
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX $DIRECTORYNAME
