<?php

namespace app\modules\api\models;


use yii\base\Model;

class RequestPasswordResetForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
        ];
    }
}