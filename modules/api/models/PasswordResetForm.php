<?php

namespace app\modules\api\models;


use yii\base\Model;

class PasswordResetForm extends Model
{
    public $password;
    public $token;

    public function rules()
    {
        return [
            [['password', 'token'], 'required'],
            ['token', 'validateToken'],
        ];
    }

    public function validateToken($attribute, $params)
    {
        $token = $this->$attribute;
        $pos = strrpos($token, '_');
        $expires = substr($token, $pos + 1);
        if ($expires < time()) {
            $this->addError($attribute, 'Invalid token.');
        }
    }

}