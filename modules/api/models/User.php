<?php

namespace app\modules\api\models;


use jakim\authserver\base\UserIdentityInterface;


class User extends \app\modelsExtended\User implements UserIdentityInterface
{

    const SCENARIO_REGISTER = 'register';

    public function fields()
    {
        switch ($this->scenario) {
            case self::SCENARIO_REGISTER:
                return ['password', 'email'];
            default:
                return [
                    'id',
                    'email',
                    'updated_at',
                    'created_at',
                ];
        }
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REGISTER] = ['email', 'password'];
        return $scenarios;
    }
    
    public static function findIdentityByCredentials($username, $password)
    {
        $security = \Yii::$app->security;
        $model = static::find()
            ->where([
                'email' => $username,
            ])->one();

        if ($model && $security->validatePassword($password, $model->password)) {
            return $model;
        }

        return null;
    }

    public static function findIdentityByRefreshToken($refreshToken)
    {
        return static::find()
            ->where([
                'refresh_token' => $refreshToken,
            ])->one();
    }

    public function setAccessToken($token)
    {
        $this->access_token = $token;
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

    public function setRefreshToken($token)
    {
        $this->refresh_token = $token;
    }

    public function getRefreshToken()
    {
        return $this->refresh_token;
    }

}
