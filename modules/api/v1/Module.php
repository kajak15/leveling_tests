<?php

namespace app\modules\api\v1;

use app\modules\api\v1\events\AuthEventsListener;
use app\modules\api\v1\events\AnswerEventsListener;
use app\modules\api\v1\models\User;
use yii\web\MultipartFormDataParser;
use yii\helpers\ArrayHelper;
use jakim\authserver\filters\HttpBearerAuth;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    const EVENT_AFTER_REQUEST_PASSWORD_RESET = 'afterRequestPasswordReset';
    const EVENT_AFTER_VOTES= 'afterVotes';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->configureApp();
        $this->initEventsListeners();
    }

    protected function configureApp()
    {
        \Yii::$app->user->enableSession = false;
        \Yii::$app->request->enableCsrfValidation = false;
        \Yii::$app->errorHandler->errorAction = '/api/default/error';
        \Yii::$app->user->identityClass = User::class;
        \Yii::$app->request->parsers['multipart/form-data'] = MultipartFormDataParser::class;
    }

    protected function initEventsListeners()
    {
        $this->on(self::EVENT_AFTER_REQUEST_PASSWORD_RESET, function ($event) {
            (new AuthEventsListener())->afterRequestPasswordReset($event);
        });
        $this->on(self::EVENT_AFTER_VOTES, function ($event) {
            (new AnswerEventsListener())->afterVotes($event);
        });
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'auth' => [
                'class' => HttpBearerAuth::class,
                'except' => ['auth/*', 'default/error']
            ]
        ]);
    }
}
