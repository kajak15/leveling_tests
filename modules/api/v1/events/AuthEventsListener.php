<?php

namespace app\modules\api\v1\events;


use yii\swiftmailer\Mailer;

class AuthEventsListener
{
    public function afterRequestPasswordReset(AuthEvent $event)
    {
        /** @var Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        return $mailer->compose('request-password-reset', $event->params)
            ->setTo($event->user->email)
            ->setSubject('Request password reset')
            ->send();
    }
}