<?php

namespace app\modules\api\v1\events;


use app\modules\api\v1\models\User;
use yii\base\Event;

class AuthEvent extends Event
{
    /**
     * @var User
     */
    public $user;

    /**
     * Additional params.
     *
     * @var array
     */
    public $params = [];
}