<?php

namespace app\modules\api\v1\controllers;

use jakim\authserver\Server;
use app\modules\api\v1\events\AuthEvent;
use app\modules\api\models\PasswordResetForm;
use app\modules\api\models\RequestPasswordResetForm;
use app\modules\api\models\User;
use app\modules\api\v1\Module;
use yii\di\Instance;
use yii\helpers\Url;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class AuthController extends Controller
{
    public $passwordResetHashTtl = 60 * 60 * 2;
    public $passwordChangeFormUri = ''/* mobile reset password link */;

    public function actionPasswordResetRedirect()
    {
        return $this->redirect($this->passwordChangeFormUri . '?token=' . \Yii::$app->request->get('token'));
    }
    
    public function actionPasswordReset()
    {
        $form = new PasswordResetForm();
        if ($form->load(\Yii::$app->request->post(), '') && $form->validate()) {
            $model = User::findOne(['password_reset_token' => $form->token]);
            if ($model === null) {
                throw new BadRequestHttpException();
            }
            $model->setNewPassword($form->password);
            if (!$model->update()) {
                \Yii::error('User model validation error: ' . print_r($model->getErrors(), true), __METHOD__);
                throw new BadRequestHttpException();
            }

            return \Yii::$app->response->setStatusCode(204);
        }

        return $form;
    }

    public function actionRequestPasswordReset()
    {
        $form = new  RequestPasswordResetForm();
        if ($form->load(\Yii::$app->request->post(), '') && $form->validate()) {
            $model = User::findOne(['email' => $form->email]);
            if ($model === null) {
                throw new BadRequestHttpException();
            }
            $model->password_reset_token = $this->generatePasswordResetHash();
            if (!$model->update()) {
                \Yii::error('User model validation error: ' . print_r($model->getErrors(), true), __METHOD__);
                throw new BadRequestHttpException();
            }

            $event = new AuthEvent();
            $event->user = $model;
            $event->params = [
                'url' => Url::to(['password-reset-redirect', 'token' => $model->password_reset_token], true),
            ];
            $this->module->trigger(Module::EVENT_AFTER_REQUEST_PASSWORD_RESET, $event);

            return \Yii::$app->response->setStatusCode(204);
        }

        return $form;
    }

    public function actionRegister()
    {
        $model = new User();
        $model->setScenario(User::SCENARIO_REGISTER);
        if ($model->load(\Yii::$app->request->getBodyParams(), '') && $model->validate()) {
            $model->setNewPassword($model->password);
            $model->generateAuthKey();
            if ($model->save()) {
                \Yii::$app->response->setStatusCode(201);
            }
        }
        return $model;
    }


    /**
     * @api {post} /auth/token
     *
     * @apiName Login
     * @apiGroup Authentication
     *
     * @apiParam {string} username User email
     * @apiParam {string} password User password
     * @apiParam {string} grant_type Just "password" value
     *
     * @apiSuccess {string} access_token Token for current session
     * @apiSuccess {string} token_type Always value "Bearer"
     * @apiSuccess {string} expires_in Token Time To Live
     * @apiSuccess {string} refresh_token Refresh token
     *
     * @return mixed
     */
    public function actionToken()
    {
        $server = Instance::ensure('authServer', Server::class);
        if (($response = $server->getResponse()) === null) {
            \Yii::$app->response->setStatusCode(400);

            return $server->error;
        }
        return $response;
    }

    /**
     * @return string
     */
    protected function generatePasswordResetHash()
    {
        return sprintf('%s_%s', \Yii::$app->security->generateRandomString(), time() + $this->passwordResetHashTtl);
    }
}