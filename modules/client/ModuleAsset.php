<?php

namespace app\modules\client;


use dmstr\web\AdminLteAsset;
use yii\web\AssetBundle;

class ModuleAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/animate.css',
        'css/admin-lte-custom.css',
    ];

    public $depends = [
        AdminLteAsset::class,
    ];
}