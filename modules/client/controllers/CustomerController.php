<?php

namespace app\modules\client\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\modules\client\models\Customer;
use app\modules\client\models\Test;
use app\modules\client\models\Level;

use yii\helpers\Url;
/**
 * Default controller for the `client` module
 */
class CustomerController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
            return $this->render('index');
    }
    
    public function actionStart()
    {
        try {
        $customer = new Customer;
        
        if ($customer->load(Yii::$app->request->post()) && $customer->validate()) 
        {
            $customer->token = \Yii::$app->security->generateRandomString(64);
            $st = $customer->save();
            
            if ($st) {
                
                // send email with link
                $emailStatus = Yii::$app->sender->sendEmail($customer);
                
                Yii::$app->response->redirect(Customer::getLink($customer), 302);
                Yii::$app->end();
            }
            
        } else {
            return $this->render('start', [
                'model' => $customer,
            ]);
        }
        
       } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', "Wystąpił błąd przy tworzeniu nowego testu");
            $customer->delete();
            throw new NotFoundHttpException(\Yii::t('app/error', "Customer cannot be saved - critical error"));
        }
       
    }
    
    
    
    public function actionTest($token)
    {
        $customer = Customer::getByToken($token);
        
        if (empty($customer))
        {
            \Yii::$app->session->setFlash('error', "Nie ma takiego testu");

//            return $this->redirect(Url::to('start'));
            Yii::$app->response->redirect(Url::to(['customer/start']), 302);
            Yii::$app->end();
        }

        if (empty($customer->tests)) 
        {
//            if customer haven't test - generate first test
            $level = Level::autoSelect();
//            vdd($level->tag);
            $testID = Test::generateTest($customer, $level);
        } else {
//            check if the test was finished
            $checkTest = $customer->getTests()->where(['score' => Null])->one();
            
            
            if (is_null($checkTest))
            {
                $verify = $this->_verifyLevel($customer);
                
                $genNextTest = $verify['genNextTest'];
                $existsTest = $verify['existsTest'];
                $level = $verify['level'];
                
                if ($genNextTest == true && $existsTest == false)
                {
    //                Create test on te next level
//                    $lastLevel = $testPassed['level_weight'];
                    $testID = Test::generateTest($customer, $level);
                    
                } else {
                    $customerLevel = Customer::checkLevel( $customer );
                    $customer->level_tag = $customerLevel->tag;
                    $customer->save();
                    
                    return $this->render('results', [
                        'level' =>  $customerLevel
                    ]);
                }
                

            } else {
//                Test is not finished
//                Remove all questions and the test. Next generate new test on the same level as prevoius test
                \app\modelsExtended\QuestionsList::deleteAll(['testID' => $checkTest->ID]);
                $checkTest->delete();
               
                $verify = $this->_verifyLevel($customer);
                $level = $verify['level'];
                
                $testID = Test::generateTest($customer, $level);
            }
        }

        
//        var_dump($customer->email, $level->tag, $st);die();
        if ( $testID != false )
        {
            $questionsList = Test::questionList($testID);
            
            return $this->render('quiz', [
                'list' => $questionsList,
                'token' => $customer->token,
                'level' => $level->name
            ]);
        }

    }
    
    private function _verifyLevel($customer) 
    {
//              Test was finished. Check if passsed
                $testPassed = Customer::checkLastPassedTest($customer);
//                Check levels
                $genNextTest = Customer::checkLevels($testPassed);
                
                $level = Level::autoSelect($lastLevel = null, $lastWeight = $testPassed['level_weight']);
                $existsTest = Customer::testOnLevelExists($customer, $level);
                
                return ['testPassed' => $testPassed, 'genNextTest' => $genNextTest, 'level' => $level, 'existsTest' => $existsTest];
    }
    
    
    public function actionAnswers($token) 
    {
        $answers = Yii::$app->request->post();
        
        foreach ($answers as $key => $answer) 
        {
            if ($key != "_csrf")
            {
                $QL = \app\modelsExtended\QuestionsList::find()->where(['ID' => $key])->one();
                $modelAnswer = \app\modelsExtended\Answers::find()->where(['ID' => $answer])->one();
                
                $QL->answer_content = $modelAnswer->content;
                $QL->correct = $modelAnswer->correct;
                $QL->save();
                
                $testID = $QL->testID;
            }
        }
        
        Test::setResults($testID);
        
        
        Yii::$app->response->redirect(Url::to(['customer/test', 'token' => $token]), 302);
        Yii::$app->end();
        
    }
    
    public function actionTimeout()
    {
        return $this->render('timeout');
    }
}
