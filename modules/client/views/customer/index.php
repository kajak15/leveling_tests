<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\client\models\Customer */
/* @var $form ActiveForm */
?>
<div class="row">
    <div class="col-md-offset-3 col-md-6 col-sm-12 text-red animated jello text-center">
        <h2><?= Yii::$app->session->getFlash('error'); ?></h2>
    </div>
</div>

<div class="row index fadeInJS">
    <div class="col-sm-12 text-center">
        <h3>Aby rozpocząć rozwiązywanie testu poziomującego, kliknij w poniższy przycisk:</h3>
    
        <a href="/start" class="btn btn-lg btn-warning btn-orange animated pulse infinite" >Rozpocznij test</a>
        
    </div>
</div>


<div class="row index info row index fadeInJS">
    <div class="col-sm-12 text-center">
        <p>Test składa się z pytań o różnym poziomie trudności - zaczynasz od <b>najłatwiejszego.</b> </p>
        <p>W momnecie gdy ukończysz dany poziom, następuje 
            weryfikacja udzielonych odpowiedzi.
        </p>
        <p>
            Jeśli zdobędziesz wymaganą liczbę punktów na danym poziomie, <br/>
            zostanie wygenerowany <b>nowy test dla wyższego poziomu</b>.
        </p>
        <p>Na rozwiązanie każdego z testów jest <b><?= TIMEOUT ?> minut</b></p>
    </div>
</div>

