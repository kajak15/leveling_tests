<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\client\models\Customer */
/* @var $form ActiveForm */

//vdd($list);


// radio: name - questionListID value - answerID
?>


<!--<form id="customer-answers-form" action="/client/customer/answers" method="POST">-->
    
<?php $form = ActiveForm::begin(['action' => Url::to(['customer/answers', 'token' => $token])]); ?>


<div class="level-info"><?= $level ?></div>
<div class="row">
    <div class="col-sm-12 text-right timer">
        <b><?= $level ?></b> [<span id="countdown"></span>]
    </div>
</div>
    
    
<div class="row quiz fadeInJS">
    <div class="col-md-offset-3 col-md-6">
        <ol>
            <?php 
            
            foreach ($list as $item) 
            { ?>
                
            <li> <div class="item">
            <?php
                echo '<span>'.$item['question'].'</span>';

                echo '<ol type="A" class="answers"><fieldset id="'.$item['questListID'].'">';
                        
                    foreach($item['answers'] as $answer) : ?>
                    
                        <li> 
                            <input type="radio" name="<?=$item['questListID']?>" value="<?= $answer['ID'] ?>"><?= $answer['content'] ?> 
                        </li>
                    <?php endforeach;
                echo '</fieldset> </ol>';
            ?>
                </div></li>
            <?php
            }
            ?>
        </ol>    
        
        <div class="form-group text-center">
            <input type="submit" value="Prześlij odpowiedzi" class="btn btn-lg btn-warning btn-orange" disabled/>
        </div>
    </div>
</div>
    
<?php ActiveForm::end(); ?>
<!--</form>-->

<script>
    $(document).ready(function () {
        $('.level-info').fadeOut(1800);
        
        $('input[type=radio]').click(function(){
            var check = true;
            $("input:radio").each(function(){
                var name = $(this).attr("name");
                if($("input:radio[name="+name+"]:checked").length == 0){
                    check = false;
                }
            });

            if(check){
                $("input[type=submit]").attr('disabled', false);
            }else{
                console.log("Not all answer");
                $("input[type=submit]").attr('disabled', true);
            }
        });
        
//        if ($("input[type=submit]").attr('disabled') == false) {
//            window.onbeforeunload = function() {
//                return "Uwaga! Wprowadzone dane zostaną utracone!";
//            }
//        }
    });
</script>



<script>
// based on: https://jsfiddle.net/qVuHW/2834/
function countdown( elementName, minutes, seconds )
{
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits( n )
    {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer()
    {
        msLeft = endTime - (+new Date);
        if ( msLeft < 1000 ) {
            console.log("redirect");
            window.location.replace("/czas");
        } else {
            time = new Date( msLeft );
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
        }
    }

    element = document.getElementById( elementName );
    endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
    updateTimer();
}

//countdown( "countdown", 1, 5 );
countdown( "countdown", <?= TIMEOUT?>, 0 );
    
</script>