<div class="row results fadeInJS" style="display: none;">
    <div class="col-sm-12 text-center">
        <h3 class="text-red">Czas minął</h3>
    </div>
</div>


<div class="row results fadeInJS info" style="display: none;">
    <div class="col-sm-12 text-center">
        <i class="fa fa-hourglass-end fa-4x iconfa animated pulse infinite" aria-hidden="true"></i><br/>
        <p>Niestety nie rozwiazano testu w określonym czasie.</p>
        <p>
            Na podany adres email został wysłany link do kolejnego testu na Twoim ostatnim poziomie.
        </p>
    </div>
</div>