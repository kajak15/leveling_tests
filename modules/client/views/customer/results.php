<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\client\models\Customer */
/* @var $form ActiveForm */
?>

<div class="row results fadeInJS" style="display: none;">
    <div class="col-sm-12 text-center">
        <h3>Twój poziom to:</h3>
        <span class="level-name">
            <?php echo ' '.$level['name'] . ' ('.$level['tag'].')'; ?>
        </span>
    </div>
</div>


<div class="row results fadeInJS info" style="display: none;">
    <div class="col-sm-12 text-center">
        <p>Gratulujemy pomyślnego rozwiązania testu.</p>
        <p>
            Niebawem skontaktujemy się z Tobą w celu dopełnienia formalności
        </p>
        <p>
            <b>Do zobaczenia wkrótce!</b>.
        </p>
    </div>
</div>

