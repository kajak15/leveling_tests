<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\client\models\Customer */
/* @var $form ActiveForm */
?>

<div class="row start fadeInJS" style="display: none;">
    <div class="col-md-offset-3 col-md-6 box-form">
        <h3>Zarejestruj się</h3>
    
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'surname') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'phone') ?>
        <div class="form-group">
            <label class="control-label" for="terms">Regulamin</label>
            <div class="form-group" id="terms">
                <div class="terms">
                    <?= $model->terms ?>
                </div>
            </div>
        </div>
            <?= $form->field($model, 'accepted')->checkbox(['required'=>"true"]);?>
            
        <div class="form-group text-center">
        <?= Html::submitButton('Zapisz dane', ['class' => 'btn btn-lg btn-warning']) ?>
        </div>    
        
        <?php ActiveForm::end(); ?>
        
    </div>
<!--    
    <div class="col-md-offset-3 col-md-6 box-loader" style="display:none;">
        Loading
    </div>-->
</div>

