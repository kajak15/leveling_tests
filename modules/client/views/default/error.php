<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="row error fadeInJS" style="display: none;">
    <div class="col-sm-12 text-center">
        <h3 class="text-red errortext">Błąd</h3>
        <i class="fa fa-bug 4x animated pulse infinite iconfa" aria-hidden="true"></i>
        <p>Niestety wystąpił błąd: <br> 
        <code><?= $message ?></code><?=$code?></p>
        
    </div>
</div>