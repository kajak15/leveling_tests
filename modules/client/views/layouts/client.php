<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

\app\modules\client\ModuleAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,200,300,400,600" rel="stylesheet"
          type="text/css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <?php $this->head() ?>
</head>
<body >
    
<?php $this->beginBody() ?>
    <noscript>
    <div class="no-script">
        Twoja przeglądarka nie wspiera JavaScript!<br/>
        <span>
            Aby skorzystać z serwisu, uruchom go w jednej z wymienionych przeglarek: Chrome, Firefox, Opera, Safari lub sprawdź ustawianie JavaScript;
        </span>
    </div>
    </noscript>
    <div class="container-fluid main-container">
        <div class="row header animated fadeInDown">
            <div class="col-md-4 text-center animated fadeInLeft">
                <a href="http://www.syllabus.com.pl"><img src="/imgs/logo.png" width="" height="" alt="Syllabus"/></a>
            </div>
            <div class="col-md-8 text-center animated fadeInRight">
                <h1> Test poziomujący </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="message">
                    <?= Yii::$app->session->getFlash('success');?>
                </div>
            </div>
        </div>
             

        <?= $content ?>
        

    </div>
    
    
    <footer class="animated fadeInUp">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">Copyright by <a href="http://www.syllabus.com.pl/">Syllabus</a> <?= date("Y"); ?></div>
        <div class="col-md-3 text-right">Magic by <a href="http://maciejborowiec.pl" target="_blank">MB</a></div>
    </footer>
    
    
<script>
    $(document).ready( function () {
        $('.fadeInJS').each(function () {
            console.log($(this));
            $(this).fadeIn( 1800);
        });
    });
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>