<?php

namespace app\modules\client\models;

use Yii;
use app\models\Questions;
use app\modelsExtended\Levels;

/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Level extends \app\modelsExtended\Levels
{
    public static function autoSelect($lastLevel = null, $lastWeight = null)
    {
        if (!is_null($lastLevel))
        {
            $lastWeight = Levels::find()->where(['tag' => $lastLevel])->one();
            
        } else if (!is_null($lastWeight)){
        
            $level = Levels::find()->where('weight > '.$lastWeight)->orderBy(['weight' => SORT_ASC])->one();
            
        } else {
            $level = Levels::find()->orderBy(['weight' => SORT_ASC])->one();
        }
        
        return $level;
    }
}