<?php

namespace app\modules\client\models;

use Yii;
use yii\helpers\Url;

use app\models\Questions;
use app\modelsExtended\Levels;
use app\modelsExtended\Tests;



/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Customer extends \app\modelsExtended\Customers
{ 
    public $terms = "
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla efficitur augue elit, 
        vel ultrices turpis consequat sed. Nullam eget augue viverra, tristique augue elementum, 
        lobortis massa. Praesent finibus luctus lacinia. Aliquam nec leo eu nisi accumsan efficitur. 
        Cras congue imperdiet nulla, quis suscipit nisi vestibulum vitae. Aenean nec posuere lacus, 
        id hendrerit magna. Nam vestibulum rutrum arcu sed eleifend. Ut et consequat libero, 
        quis euismod nulla. Ut aliquam urna quis elit ullamcorper pellentesque. Aenean rutrum porta dui, 
        lobortis ultricies sapien mattis vel. Nullam scelerisque vitae mi et vulputate. 
        Praesent condimentum nunc eu mi consequat, malesuada varius tellus varius. 
        Suspendisse sagittis augue vel enim tincidunt, id auctor massa vulputate. 
        Quisque lobortis a nisl quis tempus.</p>

        <p>Sed tincidunt tincidunt purus et scelerisque. Sed laoreet lacus vehicula viverra 
        consequat. Nunc ac lacinia libero, at rutrum magna. In malesuada metus non eros tincidunt 
        convallis. Aliquam erat volutpat. Nunc lobortis varius leo placerat tempus. In hac 
        habitasse platea dictumst. Nunc at sodales lacus. Maecenas blandit lobortis viverra. 
        Nullam at lacinia odio.</p>

        <p>Ut congue condimentum lorem, ac iaculis lectus cursus id. Proin ligula ex, laoreet ut 
        quam quis, ullamcorper convallis nulla. Duis quis facilisis arcu. Ut porta ut ligula et 
        vehicula. Donec lorem sapien, dapibus quis leo ut, iaculis vestibulum sem. Curabitur luctus 
        fermentum sem ac elementum. Donec a vestibulum massa, vitae sodales turpis. 
        Aenean a nisl augue. Nunc euismod suscipit leo facilisis ultrices. 
        Donec hendrerit finibus nunc sit amet placerat. Sed interdum tortor diam, eget 
        laoreet eros malesuada in. Duis at felis urna. Duis maximus felis in erat sollicitudin, 
        \eget rhoncus nisi placerat. Ut lobortis dictum porttitor.</p>";

    
    public static function getLink($model)
    {
        return Url::base(true).'/test/'.$model->token;
    }
    
    public static function checkLastPassedTest($customer)
    {
        $passedTests = $customer->getTests()->where(['not', ['score' => null]])->andWhere(['!=', 'passed', 0]);

        if ($passedTests->count() == 1)
        {
            return $passedTests->one();
            
        } else {
            
            $lastPassedTest = $passedTests->orderBy(['level_weight' => SORT_DESC])->one();
            
            return $lastPassedTest;
        }
    }
    
    public static function checkLevels($testPassed) 
    {
        $lastLevel = $testPassed['level_weight'];
        
        $maxLevel = Levels::find()->orderBy(['weight' => SORT_DESC])->one()['weight'];

        if ($lastLevel == $maxLevel)
        {
//            if are equal - not generate new test
            return false;
        } else {
            
            return $lastLevel;
        }
        
    }
    
    public static function checkLevel($customer)
    {
        $levelTag = $customer->getTests()->where(['passed' => 1])->orderBy(['level_weight' => SORT_DESC])->one()['level_tag'];
        if (is_null($levelTag))
        {
            $levelTag = $customer->getTests()->where(['passed' => 0])->where(['not', ['score' => NULL]])->orderBy(['level_weight' => SORT_DESC])->one()['level_tag'];
        }
        return Levels::find()->where(['tag' => $levelTag])->one();
    }
    
    
    
    public static function testOnLevelExists($customer, $nextLevel)
    {
        if (is_null($nextLevel)) return false;
        
        $allTests = $customer->getTests()->all();
        $levels = [];
        foreach ($allTests as $test )
        {
            
            $levels[] .= $test->level_tag;
        }
        
        return in_array($nextLevel->tag, $levels);
    }
}