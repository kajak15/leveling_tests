<?php

namespace app\modules\client\models;

use Yii;
use app\modules\admin\models\Questions;
use app\modelsExtended\Questions as Question;

use app\modelsExtended\Levels;
use yii\helpers\Url;

use yii\db\Expression;

use app\modelsExtended\QuestionsList;
/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Test extends \app\modelsExtended\Tests
{
    
    private static function _randomizeQuestions($level, $category) 
    {
        switch ($category) {
            case "M":
                $quest_num = $level['medium_quests_num'];
            break;
        
            case "H":
                $quest_num = $level['hard_quests_num'];
            break;
        
            default:
                $quest_num = $level['easy_quests_num'];
            break;
        }
        return $level->getQuestions()->where(['category' => $category])->orderBy(new Expression('rand()'))->limit($quest_num)->all();
    }

    
    private static function _getQuestionsList($level)
    {
        $questons_easy = Self::_randomizeQuestions($level, "E");
        $questons_medium = Self::_randomizeQuestions($level, "M");
        $questons_hard = Self::_randomizeQuestions($level, "H");
        
        return array_merge($questons_easy, array_merge($questons_medium, $questons_hard));
    }

    
    public static function generateTest($customer, $level)
    {   
        
//        $level['easy_multiplier'];
//        $level['easy_quests_num'];
//        
//        $level['medium_multiplier'];
//        $level['medium_quests_num'];
//        
//        $level['hard_multiplier'];
//        $level['hard_quests_num'];
        
        
        $questionsList = Self::_getQuestionsList($level);
        
//        get random gestions for the test
        if (empty($questionsList)) 
        {
            \Yii::$app->session->setFlash('error', "Brak pytań dla poziomu ".$level->name);
            
            Yii::$app->response->redirect(Url::base(true), 302);
            Yii::$app->end();
        }

//        create new test
        $testID = Self::_createTest($customer, $level);
        if ($testID === false) return false;
        

//        saving the randomly selected questions for the test
        foreach ($questionsList as $quest)
        {
            Self::_saveQuestion($quest, $testID, $level);
        }
        
        
        return $testID;
    }
    
    /** 
     * Function for crating test per customer for select level
     * @param object $customer
     * @param object $level 
     * @return int ID or boolean false
     * @throws NotFoundHttpException
     */
    private static function _createTest($customer, $level)
    {
        try {
            
            $test = new Self;

            $test->customerID = $customer->ID;
            $test->req_score = $level->threshold;
            $test->level_tag = $level->tag;
            $test->level_weight = $level->weight;

        
            if ($test->save())
            {
                return $test->ID;
            } else {
                return $test->save();
            }
        
        } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', "Wystąpił błąd przy tworzeniu nowego testu");
            throw new NotFoundHttpException(\Yii::t('app/error', "Customer cannot be saved - critical error"));
        }
    }
    
    /**
     * Function added question to created ealier test
     * @param object $quest
     * @param int $testID
     * @return boolean true or false
     */
    private static function _saveQuestion($quest, $testID, $level)
    {
        
//        vdd($quest);
        $model = new QuestionsList;
        $model->question_ID = $quest['ID'];
        $model->question_content = $quest['content'];
        $model->testID = $testID;
        $model->multiplier = Levels::multipliers($level)[$quest['category']];
        $model->category = $quest['category'];

        
        return $model->save();
    }
    
    public static function questionList($testID)
    {
        try { 
            $test = Self::find()->where(['ID' => $testID])->one();
            $questions = $test->getQuestionsList()->all();

            $list = [];
            foreach ($questions as $key => $question) {
                $answers = Questions::find()->where(['ID' => $question['question_ID']])->one()->getAnswers()->orderBy(new Expression('rand()'))->all();

                $list[$key] = [
                    'question' => $question['question_content'],
                    'questListID' => $question['ID'],
                    'answers' => $answers
                ];
            }

            return $list;
            
        } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', "Wystąpił błąd przy generowaniu listy pytań");
            throw new NotFoundHttpException(\Yii::t('app/error', "List of questions cannot be generated - critical error"));
        }
    }
    
    
    private static function _checkAnswers($testID)
    {
        $score = QuestionsList::find()->where(['testID' => $testID])
                                ->andWhere(['correct' => 1])
                                ->sum('multiplier');
        return $score;
    }
    
    
    public static function setResults($testID)
    {
        $score = Self::_checkAnswers($testID);
        $test = Self::find()->where(['ID' => $testID])->one();
        $test->score = $score;
        
        if ($score >= $test->req_score)
        {
            $test->passed = 1;
        }

        return $test->save();
    }
}