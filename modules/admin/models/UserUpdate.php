<?php

namespace app\modules\admin\models;

class UserUpdate extends User
{

    public $password_old;
    public $password_new1;
    public $password_new2;
    public $change_paswd;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password_old', 'password_new1', 'password_new2'], 'required', 'when' => function($model) {
                return $model->change_paswd != 0;}, 'message' => 'Pole wymagane'],
            [['email', 'password_old', 'password_new1', 'password_new2'], 'string', 'max' => 255, 'message' => "Max. 255 znaków"],
            [['email'], 'unique', 'message' => "Email musi być unikatowy! Taki użytkownik już istnieje."],
            [['password_new2'], 'compare', 'compareAttribute' => 'password_new1', 'message' => "Podane hasła różnią się"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password_old' => 'Obecne hasło',
            'password_new1' => 'Nowe hasło',
            'password_new2' => 'Powtórz nowe hasło',
            'change_paswd' => 'Zmień hasło'
        ];
    }
    
    
}




