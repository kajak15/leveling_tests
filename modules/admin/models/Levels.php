<?php

namespace app\modules\admin\models;

use Yii;
use app\models\Questions;
use app\models\Customers;


/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Levels extends \app\modelsExtended\Levels
{
        /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'name' => 'Name',
            'threshold' => 'Threshold (point)',
            'tag' => 'Tag (unique code for level)',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
    
    
    public static function countQuestion( $levelID )
    {
        return Questions::find()->where(['levelID' => $levelID])->count();
    }
    
    
    public static function countCustomers($levelTag)
    {
        return Customers::find()->where(['level_tag' => $levelTag])->count();
    }
}