<?php

namespace app\modules\admin\models;

class User extends \app\modelsExtended\User
{
    
    /** Comparison Current password wrote in form with password from database and
     *  Check that the new passwords are the same.
     * 
     * @param string $userPass
     * @param string $passNew1
     * @param string $passNew2
     * @return boolean
     */
    public function passwordComparison($userPass, $passNew1, $passNew2) 
    {
        if (empty($userPass) || empty($passNew1) || empty($passNew2)) 
        {
            \Yii::$app->session->setFlash('error', "Wypełnij wszystkie pola!");
            return false;
        }      
        
        if (!\Yii::$app->getSecurity()->validatePassword($userPass, $this->password))
        {
           \Yii::$app->session->setFlash('error', "Wpisano błędne obecne hasło!");
            return false;
        } 
        
//        var_dump($passNew1, $passNew2);die();
        if ($passNew1 !== $passNew2)
        {
           \Yii::$app->session->setFlash('error', "Podane hasła nie są identyczne!");
           return false;
        }       
              
        return true;
    }
}