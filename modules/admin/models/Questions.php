<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

use app\modules\admin\models\Levels;
/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Questions extends \app\modelsExtended\Questions
{
    public static function getLevelsArray()
    {
        $levels = Levels::find()->all();
        
        return ArrayHelper::map($levels, 'ID', 'name');
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'content' => 'Content of the Question',
            'levelID' => 'Level',
            'upadated_at' => 'Upadated At',
            'created_at' => 'Created At',
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        
        return array_merge($rules, [
            [['contentA', 'contentB', 'contentC', 'contentD'], 'required'],
        ]);
    }
    
    public static function addAnswers($model, $request) {
        
        $model->levelID = $request["Questions"]["levelID"];
        $model->contentA = $request["Questions"]["contentA"];
        $model->correctA = $request["Questions"]["correctA"];
        $model->contentB = $request["Questions"]["contentB"];
        $model->correctB = $request["Questions"]["correctB"];
        $model->contentC = $request["Questions"]["contentC"];
        $model->correctC = $request["Questions"]["correctC"];
        $model->contentD = $request["Questions"]["contentD"];
        $model->correctD = $request["Questions"]["correctD"];

        return $model;
    }
    
    public static function categories() 
    {
        return [
            'E' => "Easy",
            'M' => "Medium",
            'H' => "Hard"
        ];
    }
}