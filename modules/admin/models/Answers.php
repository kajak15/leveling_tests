<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

class Answers extends \app\modelsExtended\Answers
{
    public static function addAnswers($questionID, $request) {
        
        $modelA = new Self;
        $modelB = new Self;
        $modelC = new Self;
        $modelD = new Self;

        $modelA->content = $request["Questions"]["contentA"];
        $modelA->correct = $request["Questions"]["correctA"];
        $modelA->questionsID = $questionID;
                
        $modelB->content = $request["Questions"]["contentB"];
        $modelB->correct = $request["Questions"]["correctB"];
        $modelB->questionsID = $questionID;

        $modelC->content = $request["Questions"]["contentC"];
        $modelC->correct = $request["Questions"]["correctC"];
        $modelC->questionsID = $questionID;

        $modelD->content = $request["Questions"]["contentD"];
        $modelD->correct = $request["Questions"]["correctD"];
        $modelD->questionsID = $questionID;


        $A = $modelA->save();
        $B = $modelB->save();
        $C = $modelC->save();
        $D = $modelD->save();
        
        if($A && $B && $C && $D) {
            return true;
        }
        
        return false;
    }
    
    private static function getAnswer($id) {
        return Self::find()->where(['ID' => $id])->one();
    }
    
    
    public static function saveUpdate($model, $request) 
    {
        $answerA = Self::getAnswer($model->idA);
        $answerB = Self::getAnswer($model->idB);
        $answerC = Self::getAnswer($model->idC);
        $answerD = Self::getAnswer($model->idD);
        
        
//        var_dump($model->idA, $model->idB, $model->idC, $model->idD);die();
//        var_dump($answerA->ID, $answerB->ID, $answerC->ID, $answerD->ID);die();

        $answerA->content = $model->contentA;
        $answerA->correct = $request["Questions"]["correctA"];
        
        $answerB->content = $model->contentB;
        $answerB->correct = $request["Questions"]["correctB"];
        
        $answerC->content = $model->contentC;
        $answerC->correct = $request["Questions"]["correctC"];
        
        $answerD->content = $model->contentD;
        $answerD->correct = $request["Questions"]["correctD"];
        
//        var_dump($model->correctA);die();
        $A = $answerA->save();
        $B = $answerB->save();
        $C = $answerC->save();
        $D = $answerD->save();

        if($A && $B && $C && $D) {
            return true;
        }
        
        return false;
    }
    
    public static function deleteForQuestion($questionID) 
    {
        return \Yii::$app->db->createCommand()->delete('answers', ['questionsID' => $questionID])->execute();
    }
    
    public static function getAnswersForQuestion($questionID)
    {
        return Self::find()->where(['questionsID' => $questionID])->all();
    }
    
    public static function getAnswersToUpdate($model)
    {
        $answers = Self::getAnswersForQuestion($model->ID);
        $model->contentA = $answers[0]['content'];
        $model->correctA = $answers[0]['correct'];
        $model->idA = $answers[0]['ID'];
        
        $model->contentB = $answers[1]['content'];
        $model->correctB = $answers[1]['correct'];
        $model->idB = $answers[1]['ID'];

        $model->contentC = $answers[2]['content'];
        $model->correctC = $answers[2]['correct'];
        $model->idC = $answers[2]['ID'];

        $model->contentD = $answers[3]['content'];
        $model->correctD = $answers[3]['correct'];
        $model->idD = $answers[3]['ID'];

        return $model;
    }
}