<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-view box">

    <div class="box-body">

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary animated fadeIn']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger animated fadeIn',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
            'name',
            'surname',
            'email:email',
            'phone',
            'updated_at',
            'created_at',
            ],
            'options' => ['class' => 'table table-striped table-bordered detail-view animated fadeIn'],
        ]) ?>
    </div>
</div>
