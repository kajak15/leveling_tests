<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\client\models\Customer;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated fadeIn'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'ID',
            'name',
            'surname',
            'email:email',
            'phone',
            [
                'attribute' => 'level_tag',
                'value' => function ($model) { return app\models\Levels::find()->where(['tag' => $model->level_tag])->one()['name'] . ' ('.$model->level_tag . ')'; },
                'format' => 'raw'
            ],
            // 'updated_at',
            // 'created_at',

//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
