<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Levels */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="levels-view box">

    <div class="box-body">

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary animated fadeIn']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger animated fadeIn',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
            'name',
            'threshold',
            'weight',
            'easy_multiplier',
            'easy_quests_num',
            'medium_multiplier',
            'medium_quests_num',
            'hard_multiplier',
            'hard_quests_num',
            'tag',
            [
                'attribute' => 'updated_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->updated_at); },
                'format' => 'raw'
            ],
            // 'created_at',
            [
                'attribute' => 'created_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->created_at); },
                'format' => 'raw'
            ],
            ],
            'options' => ['class' => 'table table-striped table-bordered detail-view animated fadeIn'],
        ]) ?>
    </div>
</div>
