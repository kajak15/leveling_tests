<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LevelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use app\modelsExtended\Levels;

$this->title = 'Levels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="levels-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?= Html::a('Create Levels', ['create'], ['class' => 'btn btn-success animated fadeIn']) ?>
        </p>
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated fadeIn'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'ID',
            'name',
            'threshold',
            'tag',
//            'updated_at',
            [
                'attribute' => 'updated_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->updated_at); },
                'format' => 'raw'
            ],
            // 'created_at',
            [
                'attribute' => 'created_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->created_at); },
                'format' => 'raw'
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
