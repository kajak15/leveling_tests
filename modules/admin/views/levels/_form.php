<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Levels */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="levels-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'animated fadeIn']]); ?>

    <?php // echo $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <hr/>
    <h4>:: Test characteristics:</h4>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'threshold')->textInput(['id' => "form-threshold"]) ?>
        </div>   
        <div class="col-md-4">
            <?= $form->field($model, 'tag')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'weight')->textInput(['maxlength' => true, 'type' => "number"]) ?>
        </div>
    </div>
    <hr/>
    <h4>:: Category characteristics:</h4>
    <div class="row category">
        <div class="col-md-4 text-light-blue">
            <?= $form->field($model, 'easy_multiplier')->textInput(['id' => 'E_m', 'maxlength' => true, 'type' => "number", 'value' => 1, 'min' => 1]) ?>
            
            <?= $form->field($model, 'easy_quests_num')->textInput(['id' => 'E_q', 'maxlength' => true, 'type' => "number", 'value' => 20, 'min' => 0]) ?>
        </div>
        
        <div class="col-md-4 text-yellow">
            <?= $form->field($model, 'medium_multiplier')->textInput(['id' => 'M_m', 'maxlength' => true, 'type' => "number", 'value' => 1, 'min' => 1]) ?>

            <?= $form->field($model, 'medium_quests_num')->textInput(['id' => 'M_q', 'maxlength' => true, 'type' => "number", 'value' => 20, 'min' => 0]) ?>
        </div>
        
        <div class="col-md-4 text-red">
            <?= $form->field($model, 'hard_multiplier')->textInput(['id' => 'H_m', 'maxlength' => true, 'type' => "number", 'value' => 1, 'min' => 1]) ?>

            <?= $form->field($model, 'hard_quests_num')->textInput(['id' => 'H_q', 'maxlength' => true, 'type' => "number", 'value' => 20, 'min' => 0]) ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-sm-12 text-right">
            Threshold: <span id="min-point" style="margin-right:30px;"></span>
            Max points: <span id="max-point"></span>
        </div>
    </div>
    <?php // echo $form->field($model, 'updated_at')->textInput(['disabled'=>"true"]) ?>

    <?php // echo $form->field($model, 'created_at')->textInput(['disabled'=>"true"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    $(document).ready( function () {
        
        function calc_threshold() {
            var th = $("#form-threshold").val()*1;
            
            if (th == '' || th == null) {
                $("#min-point").text( 0 );
            } else {
                $("#min-point").text( th );
            }
            
            var sum = $("#max-point").text()*1;
            
            if (sum < th) {
                $('#min-point').addClass("text-red");
                $('.btn.btn-success').attr('disabled', true);
            } else {
                if ($('#min-point').hasClass("text-red")) {
                    $('#min-point').removeClass("text-red");
                    $('.btn.btn-success').attr('disabled', false);
                }
            }
        } 
        
        function calc_points() {
            var sum = $("#E_q").val()*$("#E_m").val() + $("#M_q").val()*$("#M_m").val() + $("#H_q").val()*$("#H_m").val();
            $("#max-point").text(sum);
        }
        
        calc_points();
        calc_threshold();
        
        $(".category input").change(function() {
            calc_points();
        });
        
        $("#form-threshold").keyup(function() {
            calc_threshold();
        });
    });
</script>
