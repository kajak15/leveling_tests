<?php

use yii\helpers\Html;
use app\widgets\InfoBox;
use app\widgets\ListBox;
use app\models\Customers;
/* @var $this yii\web\View */

$this->title = 'List of Customers by the level';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row fadeInUpBig">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach ($levels as $key => $level) :?>
                    <li class="<?= ($key == 0) ? 'active' : ''?>">
                        <a href="#tab_<?=$level->tag?>" data-toggle="tab"><?=$level->name?>
                            <span class="label label-info pull-right" style="margin: 2px 0px 0px 14px;">
                                <?= Customers::find()->where(['level_tag' => $level->tag])->count(); ?>
                            </span>
                        </a>
                    </li>
                <?php endforeach;?>
                    
                    <?php 
                    if (false) :?>
            <!--to repeat-->
                <li class="q">
                    <a href="#tab_toRepeat" data-toggle="tab">To Repeat
                        <span class="label label-warning pull-right" style="margin: 2px 0px 0px 14px;">
                            <?= Customers::find()->where(['level_tag' => null])->count(); ?>
                        </span>
                    </a>
                </li>
                <?php                endif; ?>
            </ul>
            <div class="tab-content">
                
            <?php foreach ($levels as $key => $level) :?>
                
              <div class="tab-pane <?= ($key == 0) ? 'active' : ''?>" id="tab_<?=$level->tag?>">
                  <table class="table table-striped table-bordered">
                      <tr>
                          <th>Name</th>
                          <th>Surname</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Updated at</th>
                      </tr>
                  <?php foreach ($customers as $key => $customer) :?>
                      <?php if ($customer->level_tag == $level->tag) : ?>
                      <tr>
                        <td><?=$customer->name?></td>
                        <td><?=$customer->surname?></td>
                        <td><a href="mailto:<?=$customer->email?>"><?=$customer->email?></a></td>
                        <td><?=$customer->phone?></td>
                        <td><?=$customer->updated_at?></td>
                      </tr>
                      <?php endif; ?>
                  <?php endforeach;?>
                  </table>
              </div>
                <!-- /.tab-pane -->
            <?php endforeach;?>
                
                
                <div class="tab-pane" id="tab_toRepeat">
                  <table class="table table-striped table-bordered">
                      <tr>
                          <th>Name</th>
                          <th>Surname</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Updated at</th>
                          <td><a href="#" class="btn bg-maroon">Allow on repeat ALL</a></td>
                      </tr>
                      
                      
                  <?php if (false) : foreach ($toRepeat as $key => $customer) :?>
                      <tr id="<?=$customer->ID?>">
                        <td><?=$customer->name?></td>
                        <td><?=$customer->surname?></td>
                        <td><a href="mailto:<?=$customer->email?>"><?=$customer->email?></a></td>
                        <td><?=$customer->phone?></td>
                        <td><?=$customer->updated_at?></td>
                        <td><a href="/admin/customers/allow-to-repeat?id=<?=$customer->ID?>" class="btn btn-info allow">Allow on repeat</a></td>
                      </tr>
                  <?php endforeach;                      endif;?>
                      
                      
                      
                  </table>
              </div>
                
                
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
</div>