<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tests */

$this->title = 'Create Tests';
$this->params['breadcrumbs'][] = ['label' => 'Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-create box">
    <div class="box-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
