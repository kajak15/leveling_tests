<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tests-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?= Html::a('Create Tests', ['create'], ['class' => 'btn btn-success animated fadeIn']) ?>
        </p>
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated fadeIn'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'ID',
            'customerID',
            'score',
            'req_score',
            'level_tag',
            // 'passed',
            // 'updated_at',
            // 'created_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
