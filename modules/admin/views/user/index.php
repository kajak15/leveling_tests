<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app','Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?= Html::a(\Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success animated fadeIn']) ?>
        </p>
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
            'email:email',
//            'auth_key',
//            'password',
//            'access_token',
            // 'refresh_token',
            // 'password_reset_token',
            // 'updated_at',
            // 'created_at',
            // 'user_role_id',

                ['class' => 'yii\grid\ActionColumn',
                 'template' => '{view} {update}',
                ],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
