<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Account settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view box">

    <div class="box-body">

        <p>
            <?= Html::a(\Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary animated fadeIn']) ?>
            <?php /*Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger animated fadeIn',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]); */ ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
            'email:email',
//            'auth_key',
//            'password',
//            'access_token',
//            'refresh_token',
//            'password_reset_token',
               [
                    'attribute' => 'updated_at',
                    'format'    => ['datetime', 'php:Y-m-d H:i:s'],
               ],

//            'created_at',
//            'user_role_id',
            ],
            'options' => ['class' => 'table table-striped table-bordered detail-view animated zoomIn'],
        ]) ?>
    </div>
</div>
