<?php

/**
 * @var string $name the error name
 * @var string $message the error message
 * @var \yii\base\Exception $exception the exception being handled
 */

$this->title = $name;
?>

<div class="white-box">
    <div class="alert alert-danger">
        <p class="lead">
            <?= $message ?>
        </p>
    </div>
</div>