<?php
use dmstr\widgets\Alert;
use dmstr\widgets\Menu;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var \yii\web\View $this */
/* @var $content string */

\app\modules\admin\ModuleAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,200,300,400,600" rel="stylesheet"
          type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <?php $this->head() ?>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="main-header">
        <?= Html::a('<span class="logo-mini">ST</span>Syllabus - Test', Yii::$app->homeUrl, ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">

                    <li><?= Html::a('Logout (' . Yii::$app->user->identity->email . ')', ['/admin/auth/logout']) ?></li>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <?php
            echo Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Dashboard', 'url' => ['/admin/home/dashboard'], 'icon' => 'tachometer', 'active' => $this->context->id == 'admin'],
                    ['label' => 'Customers', 'url' => ['/admin/customers/index'], 'icon' => 'users', 'active' => $this->context->id == 'admin'],
                    ['label' => 'Questions', 'url' => ['/admin/questions/index'], 'icon' => 'question-circle-o', 'active' => $this->context->id == 'admin'],
                    ['label' => 'Levels', 'url' => ['/admin/levels/index'], 'icon' => 'star-half-o ', 'active' => $this->context->id == 'admin'],
                    ['label' => 'Account Settings', 'url' => ['/admin/user-update/view?id=1'], 'icon' => 'user-circle-o', 'active' => $this->context->id == 'admin'],

//                    ['label' => 'Home', 'url' => ['/admin/home/dashboard'], 'icon' => 'fa fa-home', 'active' => $this->context->id == 'admin'],

                ],
            ]);
            ?>
        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <?php if ($this->title): ?>
                <h1><?= Html::encode($this->title); ?>
                    <?php if (isset($this->params['subTitle'])): ?>
                        <small>
                            <?= Html::encode($this->params['subTitle']) ?>
                        </small>
                    <?php endif; ?>
                </h1>
            <?php endif; ?>

            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => false,
            ]);
            ?>
        </section>
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
        <strong>
    </footer>

</div>

    
<?php require_once 'scripts.php'; ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>