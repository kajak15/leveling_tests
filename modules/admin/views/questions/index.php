<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\models\Questions;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?= Html::a('Create Questions', ['create'], ['class' => 'btn btn-success animated fadeIn']) ?>
        </p>
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated fadeIn'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'ID',
            'content:html',
//            'levelID',
            [
                'attribute' => 'levelID',
                'value' => function ($model) { return $model->getLevel()->one()['name']; },
                'format' => 'raw'
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->updated_at); },
                'format' => 'raw'
            ],
            // 'created_at',
            [
                'attribute' => 'created_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->created_at); },
                'format' => 'raw'
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
