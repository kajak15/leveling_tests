<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use app\modules\admin\models\Questions;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'animated fadeIn']]); ?>

    <?php // echo $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => [
            'rows' => 16,
            
            ],
        'preset' => 'custom',
        'clientOptions' => [
            'language' => 'en',
            'toolbarGroups' => [

		[ 'name' => 'document', 'groups' => [ 'mode', 'document', 'doctools' ] ],
		[ 'name' => 'clipboard', 'groups' => [ 'clipboard', 'undo' ] ],
		[ 'name' => 'editing', 'groups' => [ 'find', 'selection', 'spellchecker', 'editing' ] ],
		[ 'name' => 'forms', 'groups' => [ 'forms' ] ],
                '/',
                [ 'name' => 'styles', 'groups' => [ 'styles' ] ],
		[ 'name' => 'colors', 'groups' => [ 'colors' ] ],
		[ 'name' => 'basicstyles', 'groups' => [ 'basicstyles', 'cleanup' ] ],
		[ 'name' => 'paragraph', 'groups' => [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] ],
		[ 'name' => 'links', 'groups' => [ 'links' ] ],
		[ 'name' => 'insert', 'groups' => [ 'insert' ] ],
            ]    
	]

            ]);
        ?>

    <?= $form->field($model, 'category')->dropDownList( Questions::categories(), [ 'required' => 'true'] ) ?>
    
    <?php // echo $form->field($model, 'levelID')->textInput() ?>
    
    <?php
        echo $form->field($model, 'levelID')
            ->dropDownList(
                Questions::getLevelsArray(),           // Flat array ('id'=>'label')
                ['prompt'=>'select level']    // options
            );
    ?>
    
    
<!--Answers section-->  
</div>
<div class="row" >


    <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_A" data-toggle="tab" aria-expanded="true"><i class="fa fa-exclamation-triangle text-orange" style="display:none;" id="error-ans-a"></i> A <i class="fa-a text-green fa fa-check-circle" aria-hidden="true"></i></a></li>
              <li class=""><a href="#tab_B" data-toggle="tab" aria-expanded="false"><i class="fa fa-exclamation-triangle text-orange" style="display:none;" id="error-ans-b"></i> B <i class="fa-b text-red fa fa-times" aria-hidden="true"></i></a></li>
              <li class=""><a href="#tab_C" data-toggle="tab" aria-expanded="false"><i class="fa fa-exclamation-triangle text-orange" style="display:none;" id="error-ans-c"></i> C <i class="fa-c text-red fa fa-times" aria-hidden="true"></i></a></li>
              <li class=""><a href="#tab_D" data-toggle="tab" aria-expanded="false"><i class="fa fa-exclamation-triangle text-orange" style="display:none;" id="error-ans-d"></i> D <i class="fa-d text-red fa fa-times" aria-hidden="true"></i></a></li>

            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_A">
                <h3 class="pull-right">Answer <b>A</b></h3>
                <?= $form->field($model, 'correctA')->checkbox(['id' => "chbxA", 'data-s' => "A", 'class' => "chbx-answer", 'style' => "margin-right: 10px;", 'checked' => "true"])->label(false) ?>
                <?= $form->field($model, 'contentA')->textarea(['row' => '2', 'class' => "form-control"]) ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_B">
                <h3 class="pull-right">Answer <b>B</b></h3>
                <?= $form->field($model, 'correctB')->checkbox(['id' => "chbxB", 'data-s' => "B", 'class' => "chbx-answer", 'style' => "margin-right: 10px;"])->label(false) ?>
                <?= $form->field($model, 'contentB')->textarea(['row' => '2', 'class' => "form-control"]) ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_C">
                <h3 class="pull-right">Answer <b>C</b></h3>
                <?= $form->field($model, 'correctC')->checkbox(['id' => "chbxC", 'data-s' => "C", 'class' => "chbx-answer", 'style' => "margin-right: 10px;"])->label(false) ?>
                <?= $form->field($model, 'contentC')->textarea(['row' => '2', 'class' => "form-control"]) ?>
              </div>
              <!-- /.tab-pane -->
               <div class="tab-pane" id="tab_D">
                <h3 class="pull-right">Answer <b>D</b></h3>
                <?= $form->field($model, 'correctD')->checkbox(['id' => "chbxD", 'data-s' => "D", 'class' => "chbx-answer", 'style' => "margin-right: 10px;"])->label(false) ?>
                <?= $form->field($model, 'contentD')->textarea(['row' => '2', 'class' => "form-control"]) ?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
</div>



                <?= $form->field($model, 'questionsID')->hiddenInput()->label(false) ?>




    
    
    
    
    <?php // echo  $form->field($model, 'upadated_at')->textInput() ?>

    <?php // echo  $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
    if ( !$model->isNewRecord ) 
    {
        if ($model->correctA == 1)
        {
            echo "<script>var cr = 'A';</script>";
        } else if ($model->correctB == 1) {
            echo "<script>var cr = 'B';</script>";
        } else if ($model->correctC == 1) {
            echo "<script>var cr = 'C';</script>";
        }else if ($model->correctD == 1) {
            echo "<script>var cr = 'D';</script>";
        }
    } else {
        echo "<script>var cr = '0';</script>"; 
    }
?>

<script>
    $(document).ready(function(){
        
        
        $('.chbx-answer').prop('checked', false);

//        if Questions is updated
        if (cr != '0') {
            $('.chbx-answer').prop('checked', false);
            $(this).prop('checked', true);
            
            $(".nav-tabs i.fa").each(function () {
                if ($(this).hasClass('text-green')) {
                    $(this).removeClass('text-green fa-check-circle').addClass('text-red fa-times');
                }
            });

            switch (cr) 
            {
                case 'B':
                    $("nav-tabs i.fa").removeClass('text-green fa-check-circle').addClass('text-red fa-times');
                    $('.fa-b').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                    $('#chbxB').prop('checked', true);
                break;
                case 'C':
                    $('.fa-c').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                    $('#chbxC').prop('checked', true);
                break;
                case 'D':
                    $('.fa-d').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                    $('#chbxD').prop('checked', true);
                break;
                default:
                    $('.fa-a').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                    $('#chbxA').prop('checked', true);
                break;
            }
        } else {
//        Default checked is answer A

            $('#chbxA').prop('checked', true);
        }
        
        
        


//      On chhange correct answer
        $('.chbx-answer').change(function (){
            $('.chbx-answer').prop('checked', false);
            $(this).prop('checked', true);
            
            $(".nav-tabs i.fa").each(function () {
                if ($(this).hasClass('text-green')) {
                    $(this).removeClass('text-green fa-check-circle').addClass('text-red fa-times');
                }
            });

            switch ($(this).data('s')) 
            {
                case 'B':
                    $("nav-tabs i.fa").removeClass('text-green fa-check-circle').addClass('text-red fa-times');
                    $('.fa-b').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                break;
                case 'C':
                    $('.fa-c').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                break;
                case 'D':
                    $('.fa-d').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                break;
                default:
                    $('.fa-a').removeClass('text-red fa-times').addClass('text-green fa-check-circle');
                break;
            }
        });
        
//      Errors controller
        if ($('.field-questions-contenta').hasClass("has-error")) {
            console.log("Error - Ans A");
            $('#error-ans-a').fadeIn();
        }
        
        if ($('.field-questions-contentb').hasClass("has-error")) {
            console.log("Error - Ans B");
            $('#error-ans-b').fadeIn();
        }
        
        if ($('.field-questions-contentc').hasClass("has-error")) {
            console.log("Error - Ans C");
            $('#error-ans-c').fadeIn();
        }
        
        if ($('.field-questions-contentd').hasClass("has-error")) {
            console.log("Error - Ans D");
            $('#error-ans-d').fadeIn();
        }
    });
    
   
</script>
