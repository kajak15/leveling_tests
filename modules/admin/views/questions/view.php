<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\admin\models\Questions;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = substr(strip_tags($model->content), 0, 20).'...';
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view box box-info question-box">
    <div class="box-header with-border">
        <i class="fa fa-question-circle-o"></i>
        <h3 class="box-title">Question details</h3>
    </div>
    <div class="box-body">

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary animated fadeIn']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger animated fadeIn',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Create new', ['create'], ['class' => 'btn btn-success animated fadeIn pull-right']) ?>

        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
            'content:html',
            [
                'attribute' => 'levelID',
                'value' => function ($model) { return $model->getLevel()->one()['name']; },
                'format' => 'raw'
            ],
            [
                'attribute' => 'category',
                'value' => function ($model) { return Questions::categories()[$model->category]; },
                'format' => 'raw'
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->updated_at); },
                'format' => 'raw'
            ],
            // 'created_at',
            [
                'attribute' => 'created_at',
                'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->created_at); },
                'format' => 'raw'
            ],
            ],
            'options' => ['class' => 'table table-striped table-bordered detail-view animated fadeIn'],
        ]) ?>
        
        
     </div>
</div>

<div class="questions-view box box-primary answer-box">
    <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
        <h3 class="box-title">Answers</h3>
    </div>
    <div class="box-body">
        <?php 
        
            foreach ($model->getAnswers()->all() as $key => $answer) {

                echo DetailView::widget([
                    'model' => $answer,
                    'attributes' => [
                        'ID',
                    'content:html',
                    [
                        'attribute' => 'correct',
                        'value' => function ($model) { 
                            if ($model->correct == 1) 
                                return '<i class="fa-a text-green fa fa-check-circle" aria-hidden="true"></i>';
                            else 
                                return '<i class="fa-a text-red fa fa-times" aria-hidden="true"></i>';
                        },
                        'format' => 'raw'
                    ],
//                    [
//                        'attribute' => 'updated_at',
//                        'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->updated_at); },
//                        'format' => 'raw'
//                    ],
                    // 'created_at',
//                    [
//                        'attribute' => 'created_at',
//                        'value' => function ($model) { return \Yii::$app->formatter->asDatetime($model->created_at); },
//                        'format' => 'raw'
//                    ],
                    ],
                    'options' => ['class' => 'table table-striped table-bordered detail-view animated fadeIn'],
                ]);
            
                  echo "<hr/>";      
            }
            
        ?>

    </div>
</div>