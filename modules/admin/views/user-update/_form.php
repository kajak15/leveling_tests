<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\modules\admin\models\UserUpdate;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([ 'options' => ['class' => 'animated']]); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>  
    
    <?= $form->field($model, 'change_paswd')->checkbox() ?>
   
    <div id="passwd" style="display:none;">

    <?php echo $form->field($model, 'password_old')->passwordInput(['maxlength' => true, 'disabled' => "disabled"]) ?>

    <?php echo $form->field($model, 'password_new1')->passwordInput(['maxlength' => true, 'disabled' => "disabled"]) ?>
    
    <?php echo $form->field($model, 'password_new2')->passwordInput(['maxlength' => true, 'disabled' => "disabled"]) ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? \Yii::t('app',"Create User") : \Yii::t('app',"Save changes"), ['class' => $model->isNewRecord ? 'btn btn-success animated fadeIn' : 'btn btn-primary animated fadeIn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
