<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Answers */

$this->title = 'Create Answers';
$this->params['breadcrumbs'][] = ['label' => 'Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-create box">
    <div class="box-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
