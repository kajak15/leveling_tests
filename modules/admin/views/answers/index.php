<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AnswersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answers-index box">
    <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
        <p>
            <?= Html::a('Create Answers', ['create'], ['class' => 'btn btn-success animated fadeIn']) ?>
        </p>
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'options' => ['class' => 'animated fadeIn'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'ID',
            'content:ntext',
            'correct',
            'questionsID',
            'updated_at',
            // 'created_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
<?php Pjax::end(); ?>
    </div>
</div>
