<?php

namespace app\modules\admin\controllers;

use yii\web\ErrorAction;

class DefaultController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

}