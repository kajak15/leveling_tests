<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\UserUpdate;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;

/**
 * UserUpdateController implements the CRUD actions for UserUpdate model.
 */
class UserUpdateController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays a single UserUpdate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCheckpass($pass) 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $passNow = Yii::$app->user->identity->password;

        return \Yii::$app->getSecurity()->validatePassword($pass, $passNow);
    }
    /**
     * Updates an existing UserUpdate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $post = Yii::$app->request->post();   
        $model = $this->findModel($id);        
        
        if (!empty($post)) 
        {
            $model->load($post);
     
            if(!empty($post['UserUpdate']['change_paswd'])) 
            {
                $comparasionPass = $model->passwordComparison(
                    $post['UserUpdate']['password_old'],     
                    $post['UserUpdate']['password_new1'], 
                    $post['UserUpdate']['password_new2']
                    );
                
                if (!$comparasionPass)
                {
                    return $this->render('update', [ 'model' => $model ]);
                }

                $model->setNewPassword($post['UserUpdate']['password_new1']);
            }
            
            if ($model->save()) 
            {
                \Yii::$app->session->setFlash('success', \Yii::t('app',"Updated!"));

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('error', \Yii::t('app/error', "You can not change the data"));
                return $this->render('update', [ 'model' => $model ]);
            }
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the UserUpdate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserUpdate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserUpdate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app/error', 'The requested page does not exist.'));
        }
    }
}
