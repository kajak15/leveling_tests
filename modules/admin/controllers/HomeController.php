<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Category;
use app\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use app\modelsExtended\Customers;
use app\modelsExtended\Levels;


class HomeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * e
     * @return mixed
     */
    public function actionDashboard()
    {
        $levels = Levels::find()->orderBy(['weight'=>SORT_ASC])->all();
        $customers = Customers::find()->all();
        $toRepeat = Customers::find()->where(['level_tag' => null])->all();
        
        return $this->render('dashboard', [
            'levels' => $levels,
            'customers' => $customers,
            'toRepeat' => $toRepeat
        ]);
    }

}
