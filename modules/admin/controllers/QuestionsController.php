<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Questions;
use app\modules\admin\models\Answers;
use app\models\QuestionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ErrorAction;

use yii\filters\VerbFilter;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        try {
        $model = new Questions();
        $request = Yii::$app->request->post();
        
        if ($model->load($request) && $model->validate()) {
            
            $model->levelID = $request["Questions"]["levelID"];

            $question = $model->save();
            
            if ($question) 
            {
                $questionID = Questions::find()->orderBy(['id' => SORT_DESC])->one()['ID'];
                $answers = Answers::addAnswers($questionID, $request);
                
                if ($answers) {
                    return $this->redirect(['view', 'id' => $model->ID]);
                } else {
                    //Remove all created answers for the Question(by QuestionsID)
                    Answers::deleteForQuestion($questionID);                    
                    //Remove questions
                    $question->delete();
                }
                
            }
            
            //return error
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        
        } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', \Yii::t('app/error', "Error of save").': <code>'. $ex . '</code>');
                    
            throw new NotFoundHttpException(\Yii::t('app/error', "Error of save"));
        }
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        try {
            $model = $this->findModel($id);
            Answers::getAnswersToUpdate($model);
            
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                
                $question = $model->save();
                if ($question) 
                {
                    $questionID = $model->ID;
//                    vdd(Yii::$app->request->post()["Questions"]["correctA"]);
                    $answers = Answers::saveUpdate($model, Yii::$app->request->post());
                    
//                    $answers = true;
                    if ($answers) {
                        return $this->redirect(['view', 'id' => $model->ID]);
                    } else {
                        //Remove all created answers for the Question(by QuestionsID)
                        Answers::deleteForQuestion($questionID);                    
                        //Remove questions
                        $question->delete();
                    }

                }

                //return error
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', \Yii::t('app/error', "Error of save").': <code>'. $ex . '</code>');

            throw new NotFoundHttpException(\Yii::t('app/error', "Error of save"));
        }
        
        
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
        $model = $this->findModel($id);
        
            $st = Answers::deleteForQuestion(($model->ID));
        
            if ($model->delete() && $st)
            {
                \Yii::$app->session->setFlash('success', "The Question has been removed with the answers!");
            } else {
                \Yii::$app->session->setFlash('error', "The question can not be deleted - answers have been assigned");
            }
            
        return $this->redirect(['index']);
        
        } catch (Exception $ex) {
            \Yii::$app->session->setFlash('error', \Yii::t('app/error', "Error of remove question and answers").': <code>'. $ex . '</code>');

            throw new NotFoundHttpException(\Yii::t('app/error', "Error of remove question and answers"));
        }
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
