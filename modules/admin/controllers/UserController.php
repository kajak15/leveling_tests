<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\User;
//use app\modules\admin\models\User;

use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    private function _userId () {
        return \Yii::$app->user->identity->id;
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function blocked( $userId ) 
    {
        \Yii::$app->session->setFlash('error', \Yii::t('app', "Prohibited action"));
        return $this->redirect(['view', 'id' => $userId]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new UserSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
        
//        return Self::blocked(Self::_userId());
//    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        $model = new User();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
        
        return Self::blocked(Self::_userId());
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
//        $model = $this->findModel($id);
//        
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
//        {
//            
//            if ($model->save()) 
//            {
//                \Yii::$app->session->setFlash('success', "Wprowadzono zmiany!");
//
//                return $this->redirect(['view', 'id' => $model->id]);
//            } else {
//                \Yii::$app->session->setFlash('error', "Wystąpił problem ze zmianą danych!");
//
//                return $this->render('update', [ 'model' => $model ]);
//            }
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
        
        return Self::blocked(Self::_userId());
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
        return Self::blocked(Self::_userId());
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
    }
    

}
