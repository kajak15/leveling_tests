<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questions_list".
 *
 * @property int $ID
 * @property int $question_ID Create when questions are generated
 * @property string $question_content Is fill when customer choose answer
 * @property string $answer_content Is fill when customer choose answer
 * @property int $correct Is fill when customer choose answer
 * @property int $updated_at
 * @property int $created_at
 * @property int $testID
 * @property int $multiplier
 * @property string $category
 *
 * @property Tests $test
 */
class QuestionsList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['testID'], 'required'],
            [['ID', 'question_ID', 'correct', 'updated_at', 'created_at', 'testID', 'multiplier'], 'integer'],
            [['question_content', 'answer_content'], 'string'],
            [['ID'], 'unique'],
            [['category'], 'string', 'max' => 1],
            [['testID'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['testID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'question_ID' => 'Question  ID',
            'question_content' => 'Question Content',
            'answer_content' => 'Answer Content',
            'correct' => 'Correct',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'testID' => 'Test ID',
            'multiplier' => 'Multiplier',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['ID' => 'testID']);
    }
}
