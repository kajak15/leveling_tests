<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property int $ID
 * @property string $content
 * @property int $correct
 * @property int $questionsID
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Questions $questions
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['ID', 'correct', 'questionsID', 'updated_at', 'created_at'], 'integer'],
            [['content'], 'string'],
            [['ID'], 'unique'],
            [['questionsID'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['questionsID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'content' => 'Content',
            'correct' => 'Correct',
            'questionsID' => 'Questions ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasOne(Questions::className(), ['ID' => 'questionsID']);
    }
}
