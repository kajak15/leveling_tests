<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $ID
 * @property string $content
 * @property int $levelID
 * @property int $updated_at
 * @property int $created_at
 * @property string $category
 *
 * @property Levels $level
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'levelID', 'category'], 'required'],
            [['content'], 'string'],
            [['levelID', 'updated_at', 'created_at'], 'integer'],
            [['category'], 'string', 'max' => 1],
            [['levelID'], 'exist', 'skipOnError' => true, 'targetClass' => Levels::className(), 'targetAttribute' => ['levelID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'content' => 'Content',
            'levelID' => 'Level ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Levels::className(), ['ID' => 'levelID']);
    }
}
