<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tests".
 *
 * @property int $ID
 * @property int $customerID
 * @property int $score
 * @property int $req_score Treshold - backup value
 * @property string $level_tag
 * @property string $level_weight
 * @property int $passed
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Customers $customer
 */
class Tests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerID', 'req_score', 'level_tag', 'level_weight'], 'required'],
            [['ID', 'customerID', 'score', 'req_score', 'passed', 'updated_at', 'created_at', 'level_weight'], 'integer'],
            [['level_tag'], 'string', 'max' => 12],
            [['ID'], 'unique'],
            [['customerID'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customerID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'customerID' => 'Customer ID',
            'score' => 'Score',
            'req_score' => 'Req Score',
            'level_tag' => 'Level Tag',
            'level_weight' => 'Level Weight',
            'passed' => 'Passed',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['ID' => 'customerID']);
    }
    
}
