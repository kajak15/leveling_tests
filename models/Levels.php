<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 * @property int $weight
 * @property int $easy_multiplier
 * @property int $easy_quests_num
 * @property int $medium_multiplier
 * @property int $medium_quests_num
 * @property int $hard_multiplier
 * @property int $hard_quests_num
 */
class Levels extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'levels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'threshold', 'tag', 'weight', 'easy_multiplier', 'easy_quests_num', 'medium_multiplier', 'medium_quests_num', 'hard_multiplier', 'hard_quests_num'], 'required'],
            [['threshold', 'updated_at', 'created_at', 'weight', 'easy_multiplier', 'easy_quests_num', 'medium_multiplier', 'medium_quests_num', 'hard_multiplier', 'hard_quests_num'], 'integer'],
            [['name'], 'string', 'max' => 120],
            [['tag'], 'string', 'max' => 12],
            [['tag'], 'unique'],
            [['weight'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'name' => 'Name',
            'threshold' => 'Threshold',
            'tag' => 'Tag',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'weight' => 'Weight',
            'easy_multiplier' => 'Easy Multiplier',
            'easy_quests_num' => 'Easy Quests Num',
            'medium_multiplier' => 'Medium Multiplier',
            'medium_quests_num' => 'Medium Quests Num',
            'hard_multiplier' => 'Hard Multiplier',
            'hard_quests_num' => 'Hard Quests Num',
        ];
    }
}
