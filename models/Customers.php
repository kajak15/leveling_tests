<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $ID
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phone
 * @property int $updated_at
 * @property int $created_at
 * @property string $token
* @property string $level
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'phone', 'accepted'], 'required'],
            [['ID', 'updated_at', 'created_at', 'accepted', 'send_email'], 'integer'],
            [['name', 'surname', 'email', 'level_tag'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 45],
            [['token'], 'string', 'max' => 64],
            [['ID'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'phone' => 'Phone',
            'accepted' => 'Accepted',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'token' => 'Token',
            'send_email' => "Send Email",
            'level_tag' => "Level"
        ];
    }
}
