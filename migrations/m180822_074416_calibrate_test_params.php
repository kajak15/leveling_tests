<?php

use yii\db\Migration;

/**
 * Class m180822_074416_calibrate_test_params
 */
class m180822_074416_calibrate_test_params extends Migration
{
    public function up()
    {       
//        Questions table
        $this->addColumn('questions', 'category', $this->string(1)->defaultValue( "E" )); //[E]asy [M]edium [H]ard
        
        
//        Levels table (Sum of the ..._quests_num its max socre for the tests. Threnshold is mim num of socres for go to the next level.
        $this->addColumn('levels', 'easy_multiplier', $this->integer(2)->defaultValue(1)); // One qustion is equal 1 point. The point will be multiplied by multipiler.
        $this->addColumn('levels', 'easy_quests_num', $this->integer(4)->defaultValue(20)); 
        
        $this->addColumn('levels', 'medium_multiplier', $this->integer(2)->defaultValue(1)); 
        $this->addColumn('levels', 'medium_quests_num', $this->integer(4)->defaultValue(20)); 
        
        $this->addColumn('levels', 'hard_multiplier', $this->integer(2)->defaultValue(1));
        $this->addColumn('levels', 'hard_quests_num', $this->integer(4)->defaultValue(20));  
    }

    public function down()
    {
        $this->dropColumn('levels', 'easy_multiplier');
        $this->dropColumn('levels', 'easy_quests_num');
        
        $this->dropColumn('levels', 'medium_multiplier');
        $this->dropColumn('levels', 'medium_quests_num');
        
        $this->dropColumn('levels', 'hard_multiplier');
        $this->dropColumn('levels', 'hard_quests_num');
        $this->dropColumn('questions', 'category');

    }

}
