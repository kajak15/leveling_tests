<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180814_205938_basic_tables
 */
class m180814_205938_basic_tables extends Migration
{
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        
    // Questions table    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%questions}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'content'=> Schema::TYPE_TEXT.' NOT NULL',
            'levelID'=> Schema::TYPE_INTEGER.'(3) unsigned NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            ], $tableOptions);
        
        
    // Answers table    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%answers}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'content'=> Schema::TYPE_TEXT.' NOT NULL',
            'correct'=> Schema::TYPE_BOOLEAN.'(1) NOT NULL DEFAULT "0"',
            'questionsID'=> Schema::TYPE_INTEGER.'(6) unsigned NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            ], $tableOptions);
        
    
    // Levels table   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%levels}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'name'=> Schema::TYPE_STRING.'(120) NOT NULL',
            'threshold'=> Schema::TYPE_INTEGER.'(6) NOT NULL',
            'tag'=> Schema::TYPE_STRING.'(12) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            ], $tableOptions);
        
        
    // Customers table    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%customers}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'name'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'surname'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'email'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'phone'=> Schema::TYPE_STRING.'(45) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            ], $tableOptions);
        
        
    // Questions list table    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%questions_list}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'question_ID'=> Schema::TYPE_INTEGER.'(6) COMMENT "Create when questions are generated"',
            'question_content'=> Schema::TYPE_TEXT.' COMMENT "Is fill when customer choose answer"',
            'answer_content'=> Schema::TYPE_TEXT.' COMMENT "Is fill when customer choose answer"',
            'correct'=> Schema::TYPE_BOOLEAN.'(1) COMMENT "Is fill when customer choose answer"',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            'testID'=> Schema::TYPE_INTEGER.'(10) unsigned NOT NULL',
            ], $tableOptions);
    
        
    // Tests table    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tests}}', [
            'ID'=> $this->primaryKey()->unsigned(),
            'customerID'=> Schema::TYPE_INTEGER.'(10) unsigned NOT NULL',
            'score'=> Schema::TYPE_INTEGER.'(6)',
            'req_score'=> Schema::TYPE_INTEGER.'(6) NOT NULL COMMENT "Treshold - backup value"',
            'level_tag'=> Schema::TYPE_STRING.'(12) NOT NULL',
            'passed'=> Schema::TYPE_BOOLEAN.'(1) DEFAULT "0"',
            'updated_at'=> Schema::TYPE_INTEGER.'(11)',
            'created_at'=> Schema::TYPE_INTEGER.'(11)',
            ], $tableOptions);
     
        
        
        
        
        
        $this->createIndex('fk_Questions_Levels1_idx', '{{%questions}}', 'levelID', 0);
        $this->addForeignKey('fk_Questions_levelID', '{{%questions}}', 'levelID', 'Levels', 'ID');
        
        $this->createIndex('ID_UNIQUE', '{{%answers}}', 'ID', 1);
        $this->createIndex('fk_Answers_Questions_idx', '{{%answers}}', 'questionsID', 0);
        $this->addForeignKey('fk_Answers_questionsID', '{{%answers}}', 'questionsID', 'Questions', 'ID');
        
        $this->createIndex('ID_UNIQUE', '{{%levels}}', 'ID', 1);
        $this->createIndex('tag_UNIQUE', '{{%levels}}', 'tag', 1);
        
        $this->createIndex('email_UNIQUE', '{{%customers}}', 'email', 1);
        $this->createIndex('ID_UNIQUE', '{{%customers}}', 'ID', 1);
        
        $this->createIndex('ID_UNIQUE', '{{%questions_list}}', 'ID', 1);
        $this->createIndex('fk_Questions_list_Tests2_idx', '{{%questions_list}}', 'testID', 0);
        $this->addForeignKey('fk_Questions_list_testID', '{{%questions_list}}', 'testID', 'Tests', 'ID');
        
        $this->createIndex('ID_UNIQUE', '{{%tests}}', 'ID', 1);
        $this->createIndex('fk_Tests_Customers1_idx', '{{%tests}}', 'customerID', 0);
        $this->addForeignKey('fk_Tests_customerID', '{{%tests}}', 'customerID', 'Customers', 'ID');
    }

    
    public function down()
    {
        $this->dropForeignKey('fk_Tests_customerID', '{{%tests}}');
        $this->dropTable('{{%tests}}');
        
        $this->dropForeignKey('fk_Questions_list_testID', '{{%questions_list}}');
        $this->dropTable('{{%questions_list}}');
        
        $this->dropTable('{{%customers}}');
        
        $this->dropTable('{{%levels}}');
        
        $this->dropForeignKey('fk_Answers_questionsID', '{{%answers}}');
        $this->dropTable('{{%answers}}');
        
        $this->dropForeignKey('fk_Questions_levelID', '{{%questions}}');
        $this->dropTable('{{%questions}}');
    }
}
