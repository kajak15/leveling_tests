<?php

use yii\db\Migration;

class m170410_110600_add_user_role_table extends Migration
{
    public function up()
    {
        $this->createTable('user_role', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull()
        ]);
        foreach (['Admin', 'App User'] as $name) {
            $this->insert('user_role', ['name' => $name]);
        }

        $this->addColumn('user', 'user_role_id', $this->integer()->unsigned()->notNull()->defaultValue(2)); // def app user

        $this->addForeignKey('fk_user_user_role', 'user', 'user_role_id', 'user_role', 'id', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_user_user_role', 'user');
        $this->dropColumn('user', 'user_role_id');
        $this->dropTable('user_role');
    }
}
