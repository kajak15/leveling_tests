<?php

use yii\db\Migration;

class m170410_110512_add_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey()->unsigned(),
            'email' => $this->string()->unique()->notNull(),
            'auth_key' => $this->string()->unique(),
            'password' => $this->string(),
            'access_token' => $this->string()->unique(),
            'refresh_token' => $this->string()->unique(),
            'password_reset_token' => $this->string()->unique(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
