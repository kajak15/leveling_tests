<?php

use yii\db\Migration;

/**
 * Class m180822_221651_add_categry_cols_for_questions_list
 */
class m180822_221651_add_categry_cols_for_questions_list extends Migration
{
    public function up()
    {       
        $this->addColumn('questions_list', 'multiplier', $this->integer(2)); 
        $this->addColumn('questions_list', 'category', $this->string(1));
    }

    public function down()
    {
        $this->dropColumn('questions_list', 'category');
        $this->dropColumn('questions_list', 'multiplier');

    }
}
