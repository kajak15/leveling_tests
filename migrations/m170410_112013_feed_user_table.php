<?php

use yii\db\Migration;

class m170410_112013_feed_user_table extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'email' => 'admin@admin.com', 
            'auth_key' => 'HKNqXWjpOjIvMzmDbKQ0uKIewbBx6qQ_', 
            'user_role_id' => '1', 
            'password' => '$2y$10$MJJHQFiUGMVNdTpp5BgkU.9L7j2f.lNewZtb6NmFFsMC6/WsaA4uO']
        );
    }

    public function down()
    {
        echo "m170316_132500_feed_user_table cannot be reverted.\n";

        return false;
    }
}
