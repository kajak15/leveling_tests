<?php

use yii\db\Migration;

/**
 * Class m180814_214147_add_col_token_customer
 */
class m180814_214147_add_col_token_customer extends Migration
{
    public function up()
    {
        $this->addColumn('customers', 'token', $this->string(64));
        $this->addColumn('customers', 'send_email', $this->integer(1)->defaultValue( 0));
        
        $this->addColumn('levels', 'weight', $this->integer(4)->unique());


    }

    public function down()
    {
        $this->dropColumn('levels', 'weight');
        
        $this->dropColumn('customers', 'send_email');
        $this->dropColumn('customers', 'token');
    }
}
