<?php

use yii\db\Migration;

/**
 * Class m180819_001606_add_col_tests
 */
class m180819_001606_add_col_tests extends Migration
{
    public function up()
    {
        $this->addColumn('tests', 'level_weight', $this->integer(4));
    }

    public function down()
    {
        $this->dropColumn('tests', 'level_weight');
    }
}
