<?php

use yii\db\Migration;

/**
 * Class m180821_202937_fix_mariaDB
 */
class m180821_202937_fix_mariaDB extends Migration
{
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {       
        $this->alterColumn('customers', 'email', $this->string(191)->notNull()->unique());
    }

    public function down()
    {
        $this->alterColumn( 'customers','email', $this->primaryKey()->unsigned() );
    }
    
}
