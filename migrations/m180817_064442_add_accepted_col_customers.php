<?php

use yii\db\Migration;

/**
 * Class m180817_064442_add_accepted_col_customers
 */
class m180817_064442_add_accepted_col_customers extends Migration
{
    public function up()
    {
        $this->addColumn('customers', 'accepted', $this->integer(4));
    }

    public function down()
    {
        $this->dropColumn('customers', 'accepted');
    }
}
