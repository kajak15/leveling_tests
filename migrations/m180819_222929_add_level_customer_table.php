<?php

use yii\db\Migration;

/**
 * Class m180819_222929_add_level_customer_table
 */
class m180819_222929_add_level_customer_table extends Migration
{
    public function up()
    {
        $this->addColumn('customers', 'level_tag', $this->string());
    }

    public function down()
    {
        $this->dropColumn('customers', 'level_tag');
    }
}
