<?php
namespace app\widgets;

use yii\base\Widget;

class ListBox extends Widget{
	public $color = 'light';
	public $title = '';
	public $image;
	public $short = '';
	public $link = '#';
        public $classes = [];
        public $items = [];
	
	public function run(){
            $html = '<div class="box box-widget widget-user-2 animated '.implode(' ', $this->classes).'">
			<div class="widget-user-header bg-'.$this->color.'">
				<div class="widget-user-image">
				'.($this->image ? '<img class="country-box" src="'.$this->image.'" alt="'.$this->title.'">' : '').'
				</div>
				<h3 class="widget-user-username">'.$this->title.'</h3>
				<h5 class="widget-user-desc">'.$this->short.'</h5>
				<a href="'.$this->link.'" class="btn pull-right btn-success btn-flat btn-country-box">
					<i class="fa fa-angle-double-right" aria-hidden="true"></i>
				</a>
			</div>

			<div class="box-footer no-padding">
				<ul class="nav nav-stacked">';
                                foreach($this->items as $item) {
                                    $html .= '<li>'
                                            . '<a href="'.(isset($item['url']) ? $item['url'] : '#').'">'.$item['title'].''
                                            . '<span class="pull-right badge bg-'.(isset($item['color']) ? $item['color'] : 'blue').'">'.(isset($item['number']) ? $item['number'] : '')
                                            . '</span></a></li>';
                                }
				
				$html .= '</ul></div></div>';
                return $html;
	}
}
?>