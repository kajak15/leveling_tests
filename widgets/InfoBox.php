<?php
namespace app\widgets;

use yii\base\Widget;

class InfoBox extends Widget{
	public $color = 'light-blue';
	public $title = '';
	public $icon = '';
	public $number = '';
	public $classes = [];
	
	public function run(){
                return '<div class="info-box bg-'.$this->color.'-active animated '.implode(' ', $this->classes).'">'
                        . '<span class="info-box-icon bg-'.$this->color.'">'
                        . '<i class="fa fa-'.$this->icon.'"></i>'
                        . '</span>'
                        . '<div class="info-box-content">'
                        . '<span class="info-box-text">'.$this->title.'</span>'
                        . '<span class="info-box-number">'.$this->number.''
                        . '</span></div></div>';
	}
}
?>