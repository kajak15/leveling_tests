# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

set :branch, 'master'

role :web1, %w{cloud@111.111.11.111}
set :deploy_to, '/var/www/page/html'
