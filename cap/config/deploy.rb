set :application, "leveling_test"

set :repo_url, "git@bitbucket.org:kajak15/leveling_tests.git"

set :format, :airbrussh

set :log_level, :debug

set :tmp_dir, "/home/xpsyjgte/tmp_capistrano"

set :linked_files, %w{config/mailer.php config/db.php}

# append :linked_dirs, "assets"

set :keep_releases, 10

namespace :deploy do
#  before :updated, :gulp
  before :updated, :finish_steps do
    on roles(:web1) do
	 execute :php, "yii migrate"
    end
  end
end