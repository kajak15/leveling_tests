#Capistrano Template

Capistrano is a framework for building automated deployment scripts. Although Capistrano itself is written in Ruby, it can easily be used to deploy projects of any language or framework, be it Rails, Java, or PHP.

This template is valid for Capistrano version 3.9.0


## Contents

* [Installation](#installation)
* [Setup](#setup)
* [Deployment](#deployment)
* [DeploymentRollback](#deploymentrollback)


## Installation

Please follow the installation instructions found on the Capistrano website here:

```
https://capistranorb.com/documentation/getting-started/installation
```


## Setup

Alterations to this template will need to be made before it can be used for deployments.

At the time of writing, the lines to alter are described below. To note, a definition of each variable is provided in their respective files.


#### deploy.rb

```
set :repo_url, "git@dev-bitbucket/capistrano.git"        line 7
```

```
set :repo_tree, 'public'                                                                    line 10
```

```
append :linked_dirs, "assets"                                                               line 34
```

```
execute :composer, 'install'                                                                line 53
```


#### staging.rb

```
set :deploy_to, '/var/www/capistrano.dev.bitbucket/html'                          line 10
```

```
role :web, %w{capistrano@capistrano.dev.bitbucket}                            line 32
```


## Deployment

Once the setup is complete, execute the following commands to run the deployment.

#### Deploy to a staging environment

```
cap staging deploy
```

#### Deploy to a production environment

```
cap production deploy
```


## Deployment Rollback

This template has been configured to keep 10 backups, of deployed releases, in a release history on the server. To rollback to the previous version of a release, run:


#### Rollback to previous release

```
cap staging deploy:rollback
```

Or, for production environments, run:


```
cap production deploy:rollback
```

#### Rollback to specific release

To rollback to a specific release use the command:


```
cap staging deploy:rollback ROLLBACK_RELEASE=20160614133327
```

Where "20160614133327" is the release version and can be found in the release directory on the server.
