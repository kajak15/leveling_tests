<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\ErrorException;
use yii\rest\ActiveController;
use yii\helpers\Url;

use app\modules\client\models\Customer;


class Sender extends Component
{   
    /**
     * 
     * @param object $client
     * @param string $from define sender email addres
     * @param type $fileName - its sesId
     */
    public function sendEmail($model)
    {
        try 
        {
            $link = Customer::getLink($model);
            $subject = "Syllabus - test weryfikacyjny";
            
            try {

                $message = Yii::$app->mailer->compose('email', [
                            'link' => $link,
                        ])
                        ->setTo($model->email)
                        ->setSubject($subject)
                        ->send();
                
                if ($message ) {
                    $model->send_email = 1;
                    $model->save();
                }
            }  catch (\Swift_RfcComplianceException $e) {
                echo $e."\n\n";
                $message = false;
            }
                
            return $message; 
        } 
        catch (\Swift_TransportException $e) 
        {
            
        }
    }


    private function saveLogSend($msg) {
        // $path = Yii::getAlias("@reports_email");
        $path = Yii::getAlias("@app").'/reports/email/';
        $logNameExt = date('Y-M-d').'.log';
        $eol = PHP_EOL;

        $txt = '[ '.date("H:i:s").' ] '.$msg.$eol;

        $logFile = fopen($path.$logNameExt, "a+") or die("Unable to open file!");

        rewind($logFile);
        fwrite($logFile, $txt);
        fclose($logFile);
    }
}
