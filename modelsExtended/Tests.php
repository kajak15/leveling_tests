<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "Tests".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Tests extends \app\models\Tests 
{
    /**
     * Auto set timestamp
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }
    
    public function getQuestionsList()
    {
        return $this->hasMany( \app\models\QuestionsList::className(), ['testID' => 'ID']);
    }
    
    public function getWeightLevel($testLevelTag)
    {
        return \app\modules\admin\models\Levels::find()->where(['tag' => $testLevelTag])->one()['weight'];
    }
}