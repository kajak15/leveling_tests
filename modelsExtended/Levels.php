<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "levels".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Levels extends \app\models\Levels 
{
    /**
     * Auto set timestamp
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }

//    
//    public static function getQuestions($levelTag)
//    {
//        return Self::find()->where(['tag' => $levelTag])->questions->get();
//    }
    
    public function getQuestions()
    {
        return $this->hasMany(Questions::className(), ['levelID' => 'ID']);
    }
    
    
    public static function multipliers($level)
    {
        return [
            "E" => $level['easy_multiplier'],
            "M" => $level['medium_multiplier'],
            "H" => $level['hard_multiplier']
            ];
    }
}