<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "answers".
 *
 * @property int $ID
 * @property string $content
 * @property int $correct
 * @property int $questionsID
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Questions $questions
 */
class Answers extends \app\models\Answers
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }
}
