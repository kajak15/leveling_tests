<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;


class QuestionsList extends \app\models\QuestionsList
{
    /**
     * Auto set timestamp
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }
    
}
