<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customers".
 *
 * @property int $ID
 * @property string $name
 * @property int $threshold
 * @property string $tag
 * @property int $updated_at
 * @property int $created_at
 */
class Customers extends \app\models\Customers 
{
    /**
     * Auto set timestamp
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }
    
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'phone', 'accepted'], 'required', 'message' => "Powyższe pole jest wymagane"],
            [['ID', 'updated_at', 'created_at', 'accepted'], 'integer', 'message' => "Powyższe pole musi zawierać liczbę cąłkowitą"],
            [['name', 'surname', 'email'], 'string', 'max' => 255, 'message' => "Możesz podać maksymalnie 255 znaków"],
            [['phone'], 'string', 'max' => 45, 'message' => "Możesz podać maksymalnie 45 znaki"],
            [['token'], 'string', 'max' => 64],
            [['ID', 'email'], 'unique', 'message' => "Ten email już istnieje w naszej bazie danych."],
            [['email'], 'email'],
            [['phone'], 'match', 'pattern' => '/(^(?:\(?\+?48)?(?:[-\.\(\)\s]*(\d)){9}\)?$)|([0-9-+#]+)/', 'message' => "Podany numer telefonu jest"]
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'name' => 'Imię',
            'surname' => 'Nazwisko',
            'email' => 'Email',
            'phone' => 'Numer telefonu',
            'accepted' => 'Akceptuję warunki regulaminu',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'token' => 'Token',
        ];
    }
    
    public static function getByToken($token) 
    {
        return Self::find()->where(['token' => $token])->one();
    }
    
    
    public function getTests()
    {
        return $this->hasMany( Tests::className(), ['customerID' => 'ID']);
    }
}