<?php

namespace app\modelsExtended;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "questions".
 *
 * @property int $ID
 * @property string $content
 * @property int $levelID
 * @property int $upadated_at
 * @property int $created_at
 *
 * @property Levels $level
 */
class Questions extends \app\models\Questions 
{
    /**
     * Auto set timestamp
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [TimestampBehavior::className()]);
    }
    
    /**
     * Field for 
     */
    public $contentA;
    public $correctA;
    public $idA;
    
    public $contentB;
    public $correctB;
    public $idB;

    public $contentC;
    public $correctC;
    public $idC;

    public $contentD;
    public $correctD;
    public $idD;

    public $questionsID;
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return parent::getLevel();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasOne(Answers::className(), ['questionsID' => 'ID']);
    }
    
}
