<?php
function vdd($v) {
    var_dump($v);die();
}
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\v1\Module',
        ],
        'client' => [
            'class' => 'app\modules\client\Module',
        ],
    ],
    'defaultRoute' => '/admin/home/dashboard',
    'homeUrl' => '/admin/home/dashboard',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'UabXCttnfn8ad6mXvIhS9WvZc3tauovK',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/admin/auth/login',
        ],
        'errorHandler' => [
            'errorAction' => '/client/default/error',
        ],
        'sender' => [
            'class' => '\app\components\Sender',
        ],
        'mailer' => require(__DIR__ . '/mailer.php'),
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '' => 'client/customer',
                'start' => 'client/customer/start',
                'test/<token:[\w-\_]+>' => 'client/customer/test',
                'answers/<token:[\w-\_]+>' => 'client/customer/answers',
                'czas' => 'client/customer/timeout',
                'test/start' => 'client/default/error',
                'admin' => 'admin/auth/login'
            ],
        ],
        'formatter' => [
            'datetimeFormat' => 'php:d-m-Y H:i:s',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class' => \yii\gii\generators\crud\Generator::class,
                'template' => 'adminLte2',
                'templates' => [
                    'adminLte2' => '@app/generators/crud/',
                ],
            ],
        ],
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
