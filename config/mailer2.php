<?php
    return [
        'class' => 'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => false,
        'messageConfig' => [
            'from' => 'lorem.ipsum.dnd@gmail.com',
        ],
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',
            'username' => 'lorem.ipsum.dnd@gmail.com',
            'password' => 'zaq12wsxcde3',
            'port' => '587',
            'encryption' => 'tls',
        ],
    ];
